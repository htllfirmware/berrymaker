#!/bin/bash
#berryMaker.sh [-i "ARG1 ARG2 ..."] [-r "ARG1 ARG2 ..."]
#							[-e "WLAN_ESSID" -p "WLAN_PASSWORD"]
#							[-S ["SSH_PASSWORD"] ] [-K "SSH_KEY_FILE" ]
#							-s SD_CARD
#berryMaker.sh -s /dev/mmcblk0


parseArgs(){
   while getopts ":i:r:e:p:S:K:s:" opt; do
    case $opt in
      i) INSTALL_ARGS="$OPTARG"
      ;;
      r) RUN_ARGS="$OPTARG"
      ;;
      e) WLAN_ESSID="$OPTARG"
      ;;
      p) WLAN_PASSWORD="$OPTARG"
      ;;
      s) SD_CARD="$OPTARG"
      ;;
      S) SSH_PASSWORD="$OPTARG" && SSH_REQUIRED=1
			;;
      K) SSH_KEY_FILE="$OPTARG" && SSH_REQUIRED=1
      ;;
    esac
  done
}

checkArgs() {
	if [ "$#" -le 1 ]
	then
		echo './berryMaker.sh [-i "ARG1 ARG2 ..."] [-r "ARG1 ARG2 ..."]'
		echo '		[-e "WLAN_ESSID" -p "WLAN_PASSWORD"]'
		echo '		[-S ["SSH_PASSWORD"] ] [-K "SSH_KEY_FILE" ]'
		echo '		-s SD_CARD'
		exit 1
	fi
  if [ -n "$WLAN_ESSID" ]
  then
    if [ -z "$WLAN_PASSWORD" ]
		then
			echo "WLAN_ESSID or WLAN_PASSWORD is not set"
			exit 1
		fi
  fi
  if [ -n "$WLAN_PASSWORD" ]
  then
    if [ -z "$WLAN_ESSID" ]
		then
			echo "WLAN_ESSID or WLAN_PASSWORD is not set"
			exit 1
		fi
  fi
	if [ -z "$SD_CARD" ]
	then
		echo "SD_CARD is not set"
		exit 1
	fi
}

wlanCheck () {
	if [[ -n "$WLAN_ESSID" && -n "$WLAN_PASSWORD" ]] ; then WLAN_REQUIRED=1; fi
}

wpaSupplicantCheck () {
	wpa_passphrase $WLAN_ESSID $WLAN_PASSWORD > /dev/null 2>&1
	if [  $? -eq 127 ]
	then
	  echo "wpa_supplicant is not installed";
	  exit 1
	fi
}

sdCardCheck () {
	if [ ! -b $SD_CARD ]
	then
		echo "'$SD_CARD' in not block device";
		exit 1
	fi
}

sshKeyCheck () {
	if [ ! -f $SSH_KEY_FILE ]
	then
		echo "'$SSH_KEY_FILE' not found";
		exit 1
	fi
}

installFirmware () {
  read -p "Device '$SD_CARD' will be formated. Are you sure? (y/N) " -r  REPLY
  echo
  case "$REPLY" in
    y|Y ) echo "Starting..";;
    * ) echo "Halted" && exit 1 || return 1;;
  esac
  if [ ! -f 2018-06-27-raspbian-stretch-lite.zip.sha256 ]
  then
    wget https://downloads.raspberrypi.org/raspbian_lite/images/raspbian_lite-2018-06-29/2018-06-27-raspbian-stretch-lite.zip.sha256
  fi
	sha256sum -c 2018-06-27-raspbian-stretch-lite.zip.sha256 > /dev/null 2>&1
	if [  $? -eq 1 ]
	then
		echo "Downloading Raspbian.."
		wget https://downloads.raspberrypi.org/raspbian_lite/images/raspbian_lite-2018-06-29/2018-06-27-raspbian-stretch-lite.zip
	fi
	sha256sum -c 2018-06-27-raspbian-stretch-lite.zip.sha256 > /dev/null 2>&1
	if [  $? -ne 0 ]
	then
		echo "Checksum error";
    exit 1
	fi
  echo "Installing firmware..";
  unzip -o 2018-06-27-raspbian-stretch-lite.zip
	sudo dd if=./2018-06-27-raspbian-stretch-lite.img of=$SD_CARD bs=512k
}

makeRcLocal() {
	cat << EOF > "$FS_MOUNT/etc/rc.local"
#!/bin/sh -e
cd /usr/src
if [ ! -f /installed ]
then
	if [ -f /ssh ]
	then
		chpasswd < /ssh
		rm /ssh
	fi
	./install.sh $INSTALL_ARGS
	touch /installed
  reboot
fi
./run.sh $RUN_ARGS &
exit 0
EOF
}

patchFirmware () {
	echo "Patching bootloader..";
  BOOT_PARTITION=$(sudo lsblk -p --raw --output NAME | grep $SD_CARD | grep -v -e  $SD_CARD$ | head -n 1)
  BOOT_MOUNT=/tmp/forwardWlanBoot
  FS_PARTITION=$(sudo lsblk -p --raw --output NAME | grep $SD_CARD | grep -v -e  $SD_CARD$ | tail -n 1)
  FS_MOUNT=/tmp/forwardWlanFs
  mkdir $BOOT_MOUNT
  mkdir $FS_MOUNT
  sudo umount $BOOT_PARTITION
  sudo umount $FS_PARTITION
  sudo mount $BOOT_PARTITION $BOOT_MOUNT
  sudo mount $FS_PARTITION $FS_MOUNT
	if [ -n "$SSH_REQUIRED" ]
	then
  	touch "$BOOT_MOUNT/ssh"
		if [ -z "$SSH_PASSWORD" ] ; then SSH_PASSWORD=raspberry ; fi
  	echo "pi:$SSH_PASSWORD" > "$FS_MOUNT/ssh"
		chmod 700 "$FS_MOUNT/ssh"
		if [ -n "$SSH_KEY_FILE" ]
		then
			mkdir "$FS_MOUNT/home/pi/.ssh"
			cp "$SSH_KEY_FILE" "$FS_MOUNT/home/pi/.ssh/authorized_keys"
		fi
	fi
	if [ -n "$WLAN_REQUIRED" ]
	then
	  echo 'ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev' > "$BOOT_MOUNT/wpa_supplicant.conf"
	  echo 'update_config=1' >> "$BOOT_MOUNT/wpa_supplicant.conf"
	  echo 'country=RU' >> "$BOOT_MOUNT/wpa_supplicant.conf"
	  wpa_passphrase $WLAN_ESSID $WLAN_PASSWORD >> "$BOOT_MOUNT/wpa_supplicant.conf"
	fi
  cp run.sh "$FS_MOUNT/usr/src/"
  cp install.sh "$FS_MOUNT/usr/src/"
  chmod +x "$FS_MOUNT/usr/src/run.sh"
  chmod +x "$FS_MOUNT/usr/src/install.sh"
  cp -r payload "$FS_MOUNT/usr/src/"
	makeRcLocal
  sudo umount $BOOT_MOUNT
  sudo umount $FS_MOUNT
}

parseArgs "$@"
checkArgs "$@"
sdCardCheck
wlanCheck
if [ -n "$WLAN_REQUIRED" ]
then
	wpaSupplicantCheck
fi
if [ -n "$SSH_KEY_FILE" ]
then
	sshKeyCheck
fi
installFirmware
patchFirmware
